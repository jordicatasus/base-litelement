import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form';

class PersonaMain extends LitElement {

  static get properties(){
    return{
      people : {type: Array},
      showPersonForm: {type:Boolean},
      filterYearsInCompany: {type:Number}
    };
  }

  constructor() {
    super();
    this.inicializarPeople();

    this.showPersonForm=false;
  }

  render (){
      return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <h1 class="text-center">Personas</h1>
        <div class="row" id="peopleList">
          <div class="row row-cols-1 row-cols-sm-4">
          ${this.people.map(
              person => html`
              ${(this.filterYearsInCompany && this.filterYearsInCompany <= this.filterYearsInCompany)?
             html`
             <persona-ficha-listado
                fname="${person.name}"
                yearsInCompany="${person.yearsInCompany}"
                profile="${person.profile}"
                .photo="${person.photo}"
                @delete-person="${this.deletePerson}"
                @info-person="${this.infoPerson}">
                </persona-ficha-listado>` : html``}
              `
          )}
          </div>
        </div>
        <div class="row">
          <persona-form id="personForm" class="d-none border rounded border-primary"
            @persona-form-close="${this.personFormClose}"
            @persona-form-store="${this.personFormStore}"></persona-form>

        </div>
      `;
  }

  updated(changedProperties){
    console.log("Entramos en updated en persona-main");

    if(changedProperties.has("showPersonForm")){
          console.log("Cambia el valor de showPersonForm en persona-main");

      if(this.showPersonForm === true){
        this.showPersonFormData();
      }else{
        this.showPersonList();
      }
    }

    if(changedProperties.has("people")){
      console.log("Ha cambiado el valor de la propiedad people en persona-main");

      this.dispatchEvent(new CustomEvent("updated-people",{
      detail:{
        people: this.people
      }}
    ));
    }

    if(changedProperties.has("filterYearsInCompany")){
      console.log("Ha cambiado el valor de la propiedad filterYearsInCompany en persona-main"+this.filterYearsInCompany);
      this.inicializarPeople();
      this.people = this.people.filter(
        person => person.yearsInCompany <= this.filterYearsInCompany
     );
    }





  }

  personFormClose(){
    console.log("personFormClose");
    console.log("Se ha cerrado el formulario de la persona");

    this.showPersonForm = false;
  }

  personFormStore(e){
    console.log("personFormStore");
    console.log("Se va a almacenar una persona");

    console.log("Name de la persona es "+ e.detail.person.name);
    console.log("Profile de la persona es "+ e.detail.person.profile);
    console.log("YearsInCompany de la persona es "+ e.detail.person.yearsInCompany);

    if(e.detail.edittingPerson === true){
      console.log("Se va a actualizar la persona de nombre"+ e.detail.person.name);

      this.people = this.people.map(
        person => person.name === e.detail.person.name
        ? person = e.detail.person : person);

    }else{
      console.log("Se va a guardar una persona nueva");
      this.people = [...this.people,e.detail.person]
    }

    console.log("Persona almacenada");

    this.showPersonForm = false;
  }

  showPersonList(){
    console.log("showPersonList");
    console.log("Mostrando el listado de personas");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
  }

  showPersonFormData(){
    console.log("showPersonFormData");
    console.log("Mostrando formulario de persona");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
  }


  deletePerson(e){
    console.log("delete en persona-main");
    console.log("Borramos "+e.detail.name);

    this.people = this.people.filter(
      person => person.name != e.detail.name
    );
  }

  infoPerson(e){
    console.log("infoPerson");
    console.log("Se ha pedido más información de la persona "+e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    );

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById("personForm").person=person;
    this.shadowRoot.getElementById("personForm").edittingPerson = true;
    this.showPersonForm = true;
  }

  inicializarPeople(){
     this.people = [
      {
        name : "Pepe",
        yearsInCompany : 10,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/img.jpg",
          alt: "Pepe"
        }
      },
      {
        name : "Jose",
        yearsInCompany : 9,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/img.jpg",
          alt: "Jose"
        }
      },
      {
        name : "Paco",
        yearsInCompany : 8,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/img.jpg",
          alt: "Paco"
        }
      },
      {
        name : "Manolo",
        yearsInCompany : 8,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/img.jpg",
          alt: "Manolo"
        }
      },
      {
        name : "Juanca",
        yearsInCompany : 4,
        profile: "Lorem ipsum dolor sit amet.",
        photo: {
          src: "./img/img.jpg",
          alt: "Juanca"
        }
      }

    ];
  }

}

customElements.define('persona-main', PersonaMain);
