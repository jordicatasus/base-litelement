import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {

  static get properties(){
    return{
        course: {type: String},
        year: {type: String}

    };
  }

  constructor() {
    super();
  }



  render (){
      return html`
      <h1>Gestor Evento</h1>
      <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
      <receptor-evento></receptor-evento>
      `;
  }

  processEvent(e){
    console.log("processEvent");
    console.log(e);
  }


}

customElements.define('gestor-evento', GestorEvento);
